import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  @Input() texto = 'Cargando...';
  public folder: string;
  product = [
    {
      calzadoId: '1',
      nombeCalzado: 'Tenis',
      color: 'Blancos',
      rutaCalzado: 'https://fotos.subefotos.com/2712a5ef7f8aefecc20660041c091a76o.jpg',
      estadoCalzado: 'Semi nuevo',
      talla: '6',
      precio: '$250',
      ubicacion: 'San Isidro Boxipe',
      contacto: '5615548358',
      nombrePropietario: 'Luis Medina',
    },
    {
      calzadoId: '2',
      nombeCalzado: 'Sudadera',
      color: 'roja',
      rutaCalzado: 'https://fotos.subefotos.com/e4c8ad438a378106e4fc741d1c99a087o.jpg',
      estadoCalzado: 'Nuevo',
      talla: 'G',
      precio: '$450',
      ubicacion: 'San Juan',
      contacto: '3221448358',
      nombrePropietario: 'Andrea Perez',
    },
    {
      calzadoId: '3',
      nombeCalzado: 'Abrigo',
      color: 'Negro',
      rutaCalzado: 'https://fotos.subefotos.com/f1c97c7469d60abc9e9c94f00769fb26o.jpg',
      estadoCalzado: 'Semi nuevo',
      talla: 'G',
      precio: '$650',
      ubicacion: 'San Felipe',
      contacto: '9615548353',
      nombrePropietario: 'Alberto Davila',
    },
    {
      calzadoId: '4',
      nombeCalzado: 'Blusa',
      color: 'Rosa',
      rutaCalzado: 'https://fotos.subefotos.com/72db33cf7c30265d5eea164721e2fb4eo.jpg',
      estadoCalzado: 'Nuevo',
      talla: 'MD',
      precio: '$350',
      ubicacion: 'Ixtlahuaca',
      contacto: '2612248358',
      nombrePropietario: 'Juan Sanchez',
    },
    {
      calzadoId: '5',
      nombeCalzado: 'Abrigo',
      color: 'Negro',
      rutaCalzado: 'https://fotos.subefotos.com/f1c97c7469d60abc9e9c94f00769fb26o.jpg',
      estadoCalzado: 'Semi nuevo',
      talla: 'G',
      precio: '$650',
      ubicacion: 'San Migule',
      contacto: '7615548358',
      nombrePropietario: 'Daniel Mendoza',
    },
    {
      calzadoId: '6',
      nombeCalzado: 'Blusa',
      color: 'Rosa',
      rutaCalzado: 'https://fotos.subefotos.com/72db33cf7c30265d5eea164721e2fb4eo.jpg',
      estadoCalzado: 'Nuevo',
      talla: 'MD',
      precio: '$350',
      ubicacion: 'Santa Ana',
      contacto: '6715548358',
      nombrePropietario: 'Yessica Gonzales',
    },
    {
      calzadoId: '7',
      nombeCalzado: 'Sudadera',
      color: 'roja',
      rutaCalzado: 'https://fotos.subefotos.com/e4c8ad438a378106e4fc741d1c99a087o.jpg',
      estadoCalzado: 'Nuevo',
      talla: 'G',
      precio: '$450',
      ubicacion: 'Santa Maria',
      contacto: '8615548358',
      nombrePropietario: 'Jose Juan',
    },
    {
      calzadoId: '8',
      nombeCalzado: 'Abrigo',
      color: 'Negro',
      rutaCalzado: 'https://fotos.subefotos.com/f1c97c7469d60abc9e9c94f00769fb26o.jpg',
      estadoCalzado: 'Semi nuevo',
      talla: 'G',
      precio: '$650',
      ubicacion: 'San Pedro',
      contacto: '1615548358',
      nombrePropietario: 'Alejandro Sanchez',
    },
    {
      calzadoId: '9',
      nombeCalzado: 'Tenis',
      color: 'Blancos',
      rutaCalzado: 'https://fotos.subefotos.com/2712a5ef7f8aefecc20660041c091a76o.jpg',
      estadoCalzado: 'Semi nuevo',
      talla: '6',
      precio: '$250',
      ubicacion: 'San Isidro',
      contacto: '8615548358',
      nombrePropietario: 'Karen Miranda',
    },
    {
      calzadoId: '10',
      nombeCalzado: 'Sudadera',
      color: 'roja',
      rutaCalzado: 'https://fotos.subefotos.com/e4c8ad438a378106e4fc741d1c99a087o.jpg',
      estadoCalzado: 'Nuevo',
      talla: 'G',
      precio: '$450',
      ubicacion: 'Sa Mateo',
      contacto: '5615548358',
      nombrePropietario: 'Pedro Mendez',
    },
    {
      calzadoId: '1',
      nombeCalzado: 'Tenis',
      color: 'Blancos',
      rutaCalzado: 'https://fotos.subefotos.com/2712a5ef7f8aefecc20660041c091a76o.jpg',
      estadoCalzado: 'Semi nuevo',
      talla: '6',
      precio: '$250',
      ubicacion: 'Ixtlahuaca',
      contacto: '5515548358',
      nombrePropietario: 'Ines Martinez',
    },
    {
      calzadoId: '2',
      nombeCalzado: 'Sudadera',
      color: 'roja',
      rutaCalzado: 'https://fotos.subefotos.com/e4c8ad438a378106e4fc741d1c99a087o.jpg',
      estadoCalzado: 'Nuevo',
      talla: 'G',
      precio: '$450',
      ubicacion: 'San Agustin',
      contacto: '6615548358',
      nombrePropietario: 'Ramon Ayala',
    },
    {
      calzadoId: '3',
      nombeCalzado: 'Abrigo',
      color: 'Negro',
      rutaCalzado: 'https://fotos.subefotos.com/f1c97c7469d60abc9e9c94f00769fb26o.jpg',
      estadoCalzado: 'Semi nuevo',
      talla: 'G',
      precio: '$650',
      ubicacion: 'Santa Ana',
      contacto: '9615548358',
      nombrePropietario: 'Andrea Mendoza',
    },
    {
      calzadoId: '4',
      nombeCalzado: 'Blusa',
      color: 'Rosa',
      rutaCalzado: 'https://fotos.subefotos.com/72db33cf7c30265d5eea164721e2fb4eo.jpg',
      estadoCalzado: 'Nuevo',
      talla: 'MD',
      precio: '$350',
      ubicacion: 'Jocotitlan',
      contacto: '2615548358',
      nombrePropietario: 'Miguel Andres',
    },
    {
      calzadoId: '5',
      nombeCalzado: 'Abrigo',
      color: 'Negro',
      rutaCalzado: 'https://fotos.subefotos.com/f1c97c7469d60abc9e9c94f00769fb26o.jpg',
      estadoCalzado: 'Semi nuevo',
      talla: 'G',
      precio: '$650',
      ubicacion: 'San Pedro',
      contacto: '3315548358',
      nombrePropietario: 'Alondra Cruz',
    },
    {
      calzadoId: '6',
      nombeCalzado: 'Blusa',
      color: 'Rosa',
      rutaCalzado: 'https://fotos.subefotos.com/72db33cf7c30265d5eea164721e2fb4eo.jpg',
      estadoCalzado: 'Nuevo',
      talla: 'MD',
      precio: '$350',
      ubicacion: 'San Miguel',
      contacto: '5615548358',
      nombrePropietario: 'Julian Medina',
    },
    {
      calzadoId: '7',
      nombeCalzado: 'Sudadera',
      color: 'roja',
      rutaCalzado: 'https://fotos.subefotos.com/e4c8ad438a378106e4fc741d1c99a087o.jpg',
      estadoCalzado: 'Nuevo',
      talla: 'G',
      precio: '$450',
      ubicacion: 'San Isidro',
      contacto: '5565548358',
      nombrePropietario: 'Fernando Andil',
    },
    {
      calzadoId: '8',
      nombeCalzado: 'Abrigo',
      color: 'Negro',
      rutaCalzado: 'https://fotos.subefotos.com/f1c97c7469d60abc9e9c94f00769fb26o.jpg',
      estadoCalzado: 'Semi nuevo',
      talla: 'G',
      precio: '$650',
      ubicacion: 'Atlacomulco',
      contacto: '8787548358',
      nombrePropietario: 'Jaun Medina',
    },
    {
      calzadoId: '9',
      nombeCalzado: 'Tenis',
      color: 'Blancos',
      rutaCalzado: 'https://fotos.subefotos.com/2712a5ef7f8aefecc20660041c091a76o.jpg',
      estadoCalzado: 'Semi nuevo',
      talla: '6',
      precio: '$250',
      ubicacion: 'Emiliano Zapata',
      contacto: '9619548358',
      nombrePropietario: 'Jose Carlos',
    },
    {
      calzadoId: '10',
      nombeCalzado: 'Sudadera',
      color: 'roja',
      rutaCalzado: 'https://fotos.subefotos.com/e4c8ad438a378106e4fc741d1c99a087o.jpg',
      estadoCalzado: 'Nuevo',
      talla: 'G',
      precio: '$450',
      ubicacion: 'Toluza',
      contacto: '3315548358',
      nombrePropietario: 'Evelin Mateo',
    }
  ]

  constructor(private activatedRoute: ActivatedRoute, private screen: ScreenOrientation, private navCtrl: NavController) { 
    this.screen.lock(this.screen.ORIENTATIONS.PORTRAIT);
    this.screen.onChange().subscribe(
      () => { /**/});
  }
productos;
loader = false;
  ngOnInit() {
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
     this.productos = this.product;
    }, 3000);
  }
  ocul = false;
  nombre;
  color;
  precio;
  nombreProp;
  contacto;
  imagen;
  estado;
  lugar;
  valores(val){
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.ocul = true;  
      this.nombre = val.nombeCalzado;
      this.color = val.color;
      this.precio = val.precio;
      this.nombreProp = val.nombrePropietario;
      this.contacto = val.contacto;
      this.imagen = val.rutaCalzado;
      this.estado = val.estadoCalzado;
      this.lugar = val.ubicacion;
    }, 3700);
    console.log("Datos: ",val);
  }

  cancel(){
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.ocul = false;
    }, 2000);
  }
  Ubicacion_art(){
    this.ocul = false;
    this.navCtrl.navigateForward('/mapa');
  }

}
