import { Component, OnInit } from '@angular/core';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { GoogleMaps, GoogleMap, GoogleMapsEvent, Marker, GoogleMapsAnimation, MyLocation, GoogleMapsMapTypeId} from "@ionic-native/google-maps";
import { ToastController, LoadingController } from '@ionic/angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.page.html',
  styleUrls: ['./mapa.page.scss'],
})
export class MapaPage implements OnInit {
  map: GoogleMap;
  loading: any;
  lat;
  lon;
  constructor(private geo: Geolocation, public loadingCtrl: LoadingController, 
    public toastCtrl: ToastController, private screen: ScreenOrientation) {
      this.screen.lock(this.screen.ORIENTATIONS.PORTRAIT);
    this.screen.onChange().subscribe(
      () => { /**/});
     }

  ngOnInit() {
    this.getGeolocation();
  }
  
  getGeolocation(){
    this.geo.getCurrentPosition().then((geoposition: Geoposition)=>{
      this.lat = geoposition.coords.latitude;
      this.lon = geoposition.coords.longitude;
      console.log("Latitud: "+this.lat+"Longitud: "+this.lon);
      this.loadMap();
    });
  }
  

  loadMap() {
    this.map = GoogleMaps.create("map_canvas", {
      camera: {
        target: {
          lat: this.lat,
          lng: this.lon
        },
        zoom: 18,
        tilt: 30,
      }
    });
  }
  async localizar() {
    this.map.clear();
    this.loading = await this.loadingCtrl.create({
      message: "Espera por favor..."
    });
    await this.loading.present();
    this.map
      .getMyLocation()
      .then((location: MyLocation) => {
        this.loading.dismiss();
        this.map.animateCamera({
          target: location.latLng,
          zoom: 17,
          tilt: 30
        });

        let marker: Marker = this.map.addMarkerSync({
          title: "Estoy aquí!",
          position: location.latLng,
          animation: GoogleMapsAnimation.BOUNCE
        });

        marker.showInfoWindow();
        marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
          this.showToast("clicked!");
        });
      })
      .catch(error => {
        this.loading.dismiss();
        this.showToast(error.error_message);
      });
  }
  async showToast(mensaje) {
    let toast = await this.toastCtrl.create({
      message: mensaje,
      duration: 2000,
      position: "bottom"
    });

    toast.present();
  }


}
