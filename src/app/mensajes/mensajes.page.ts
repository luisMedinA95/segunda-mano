import { Component, OnInit } from '@angular/core';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  selector: 'app-mensajes',
  templateUrl: './mensajes.page.html',
  styleUrls: ['./mensajes.page.scss'],
})
export class MensajesPage implements OnInit {

  constructor(private screen: ScreenOrientation) { 
    this.screen.lock(this.screen.ORIENTATIONS.PORTRAIT);
    this.screen.onChange().subscribe(
      () => { /**/});
  }

  ngOnInit() {
  }

}
